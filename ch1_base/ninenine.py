"""
打印九九乘法表
"""
# range(start,stop,stop):不包括stop,step默认是1
for i in range(1, 10):
    # i+1的原因是因为range()左闭右开，不包括stop
    for j in range(1, i + 1):
        print(f'{j}*{i}={j * i}', end='\t')

    # print默认结束符就是\n
    print()
