# 运算符
print('------变量的定义------')
a=3
print(type(3))
print('-----算数运算符-----')
a1,a2=6,4
print(a1/a2)
#取模/求余数
print(a1%a2)
# python有整除
print(f'余数:{a1//a2}')
# 指数，幂
print(f'指数:{2**4}')
print(f'指数:{pow(2,5)}')