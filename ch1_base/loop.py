while True:
    print()
    print()

i = 2
while i > 5:
    print()
    print()
    i = i + 1

#      1~4
for i in range(5):
    print()

#      2,3,4
for i in range(2,5):
    print()

#      13579
for i in range(1,10,2):
    print()