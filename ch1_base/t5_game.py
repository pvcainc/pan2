"""
当猜对了就结束游戏，否则一直进行下去：
1、电脑产生一个随机数 0-9
2、循环操作：直到猜对为止
   1）让用户输入一个数字
   2）比较用户输入的值与电脑产生的数字大小，提示：猜大或猜小或猜对
"""
import random

ran = random.randint(0,10)
print('猜数字游戏！！！')

# 当循环次数不确定是，  一般用while，否则一般用for

while True:
    guess=eval(input('请输入猜测的数字'))
    if guess<ran:
        print('猜小了')
    elif guess>ran:
        print('猜大了')
    else:
        print('恭喜，猜对了！！')

        #break 跳出循环
        break